
package frames;

import javax.swing.JOptionPane;

/**
 *
 * @Emmanuel Alejandro Alonso Guitron
 * @Juan Pablo Alonso Guitron 
 * @version 1.0
 */
public class NewGuest extends javax.swing.JFrame {


    public NewGuest() {
        initComponents();
        this.setTitle("NEW GUEST");
        setLocationRelativeTo(null);
        Active.add(INACTIVE);
        Active.add(ACTIVE);
        Type.add(Teacher);
        Type.add(Student);
        Type.add(Auxiliar);
        ACTIVE.doClick();
        Teacher.doClick();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Type = new javax.swing.ButtonGroup();
        Active = new javax.swing.ButtonGroup();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        Gender = new javax.swing.JComboBox();
        FirstName = new javax.swing.JTextField();
        LastName = new javax.swing.JTextField();
        Address = new javax.swing.JTextField();
        Phone = new javax.swing.JTextField();
        Speciality = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        Teacher = new javax.swing.JRadioButton();
        Student = new javax.swing.JRadioButton();
        Auxiliar = new javax.swing.JRadioButton();
        BSave = new javax.swing.JButton();
        ACTIVE = new javax.swing.JRadioButton();
        INACTIVE = new javax.swing.JRadioButton();
        jLabel15 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setText("CANCEL");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 410, -1, -1));

        jLabel1.setFont(new java.awt.Font("Segoe UI Black", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("NEW GUEST");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 10, -1, -1));

        jLabel2.setFont(new java.awt.Font("Arial Black", 0, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Insert the requested information:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, -1, -1));

        jLabel8.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("First Name:");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, -1, -1));

        jLabel9.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Last Name:");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, -1, -1));

        jLabel10.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Address:");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 200, -1, -1));

        jLabel11.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Phone Number:");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 240, -1, -1));

        jLabel12.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Speciality:");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 280, -1, -1));

        jLabel13.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Gender:");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, -1, -1));

        Gender.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Male", "Female", "Undefined" }));
        Gender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GenderActionPerformed(evt);
            }
        });
        getContentPane().add(Gender, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, -1, -1));
        getContentPane().add(FirstName, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 80, 269, -1));
        getContentPane().add(LastName, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 120, 270, -1));
        getContentPane().add(Address, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 190, 229, -1));
        getContentPane().add(Phone, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 230, 123, -1));

        Speciality.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AACH", "TICR", "MTMF", "TICS", "DNAM", "LI", "MTAA", "DDAA" }));
        getContentPane().add(Speciality, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 270, -1, -1));

        jLabel14.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Type:");
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 360, -1, -1));

        Type.add(Teacher);
        Teacher.setForeground(new java.awt.Color(255, 255, 255));
        Teacher.setText("Teacher");
        getContentPane().add(Teacher, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 320, -1, -1));

        Type.add(Student);
        Student.setForeground(new java.awt.Color(255, 255, 255));
        Student.setText("Student");
        getContentPane().add(Student, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 350, -1, -1));

        Type.add(Auxiliar);
        Auxiliar.setForeground(new java.awt.Color(255, 255, 255));
        Auxiliar.setText("Auxiliar");
        getContentPane().add(Auxiliar, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 390, -1, -1));

        BSave.setText("SAVE");
        BSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BSaveActionPerformed(evt);
            }
        });
        getContentPane().add(BSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 340, 159, 48));

        Active.add(ACTIVE);
        ACTIVE.setForeground(new java.awt.Color(255, 255, 255));
        ACTIVE.setText("Active");
        getContentPane().add(ACTIVE, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 240, -1, -1));

        Active.add(INACTIVE);
        INACTIVE.setForeground(new java.awt.Color(255, 255, 255));
        INACTIVE.setText("Inactive");
        getContentPane().add(INACTIVE, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 270, -1, -1));

        jLabel15.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Status");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 260, -1, -1));

        jLabel3.setFont(new java.awt.Font("Segoe UI Black", 0, 14)); // NOI18N
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/palmas.jpg"))); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        MainWindow m=new MainWindow();
        m.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void GenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GenderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_GenderActionPerformed

    private void BSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BSaveActionPerformed
        String firstName=(FirstName.getText());
        String lastName=(LastName.getText());
        String gender=(String) (Gender.getSelectedItem());
        String address=(Address.getText());
        String phone=(Phone.getText());
        String speciality=(String) (Speciality.getSelectedItem());
        String status="";
        String type="";
        if(ACTIVE.isSelected())
            status="Active";
        else
            status="Inactive";
        
        if(Teacher.isSelected())
            type="Teacher";
        else if(Student.isSelected())
            type="Student";
        else
            type="Auxiliar";
        
        if (firstName.isEmpty() || lastName.isEmpty() || address.isEmpty() || phone.isEmpty())
        {
                if(firstName.equals("") || firstName.isEmpty())
              {
                  JOptionPane.showMessageDialog(null,"'First Name' is empty","ERROR",JOptionPane.ERROR_MESSAGE);
              }
              if(lastName.equals("") || lastName.isEmpty())
              {
                  JOptionPane.showMessageDialog(null,"'Last Name' is empty","ERROR",JOptionPane.ERROR_MESSAGE);
              }
              if(address.equals("") || address.isEmpty())
              {
                  JOptionPane.showMessageDialog(null,"'Address' is empty","ERROR",JOptionPane.ERROR_MESSAGE);
              }
              if(phone.equals("") || phone.isEmpty())
              {
                  JOptionPane.showMessageDialog(null,"'Phone Number' is empty","ERROR",JOptionPane.ERROR_MESSAGE);
              }
        }
        else
        {
            JOptionPane.showMessageDialog(null,"SAVED INFORMATION","SUCCESS",JOptionPane.INFORMATION_MESSAGE);
            System.out.println("FN="+firstName+"\nLN= "+lastName+"\nphone= "+phone+"\nstatus= "+status+"\ntype= "+type);
            System.out.println("\ngender= "+gender+" speciality= "+speciality);
            MainWindow mw=new MainWindow();
            mw.setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_BSaveActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton ACTIVE;
    private javax.swing.ButtonGroup Active;
    private javax.swing.JTextField Address;
    private javax.swing.JRadioButton Auxiliar;
    public static javax.swing.JButton BSave;
    private javax.swing.JTextField FirstName;
    private javax.swing.JComboBox Gender;
    private javax.swing.JRadioButton INACTIVE;
    private javax.swing.JTextField LastName;
    private javax.swing.JTextField Phone;
    private javax.swing.JComboBox Speciality;
    private javax.swing.JRadioButton Student;
    private javax.swing.JRadioButton Teacher;
    private javax.swing.ButtonGroup Type;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    // End of variables declaration//GEN-END:variables
}
